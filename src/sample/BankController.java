package sample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;




public class BankController {
 BankAccount BankAc;

 private static final double INITIAL_BALANCE = 1000;   

public static void main(String[] args) {
	// TODO Auto-generated method stub
	new BankController();
	
}

public BankController() {
	BankAc = new BankAccount(INITIAL_BALANCE);
	frame = new InvestmentFrame();
	 frame.createTextField();
	 list = new ListenerMgr();
	 frame.createButton(list);
	 frame.createPanel();
	 frame.getText(getBalance());
     frame.setVisible(true);
	


}


public void deposit(double amount)
	{  
		BankAc.deposit(amount);
	}

public void withdraw(double amount)
	{   
		BankAc.withdraw(amount);
	}

public double getBalance()
{   
   return BankAc.getBalance();
}


class ListenerMgr implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
	      double interest = getBalance() * frame.getRate() / 100;
	      deposit(interest);
	      frame.getText(getBalance());
	      


	}

}

ActionListener list;
 InvestmentFrame frame;
}
